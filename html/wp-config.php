<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ae126naqdv_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ae126naqdv');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'yBuntrVV');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8MI0s0W=9e-FU-M*2y-cv/z]|%<.)NgP#g$t%X(5f7OaoXTfl73:>~E{>iZD;B=l');
define('SECURE_AUTH_KEY',  'c0D,I{P|ndH[<$7;$UC#^E6Dx6ZC~?/3fUE|Tm|wo#%pO{R8?Ru*&c+p k_@~+eV');
define('LOGGED_IN_KEY',    'N+FW;{{-[b1?/w-jO.;?~r=qk,.r,h,A$QWLU3vZWS>_)C1PKL)r>Sx=)uf[ #>M');
define('NONCE_KEY',        'K@/*^,qWHYMG-zfhq3?4[|%+Oa^TNQpO_/y9d-03E6W2T3k-KwRI+w:Fsy|rOQbs');
define('AUTH_SALT',        '!T.C&G&V +1x|Y(bCdy`6^aVW<yKKO)D-B(Z#ZZUMNy]Z)brsCG9,T ^M|g,l}aX');
define('SECURE_AUTH_SALT', '1-VQ-kuJo@7.FU-F7~P6Ic^e7b91=`>/m.R>p|A|$#ioVMI-op#|_7W?x$PNB.C#');
define('LOGGED_IN_SALT',   'ToKKF|4&;)>9/4AdeI)z2~6{e.K|1p-c?C<NZTq?T7>5Tq>Ui-9+Z_[Hm+2Q${$}');
define('NONCE_SALT',       '-lD>N]VlhNA#J;DFH82<-pfS:bVb$68p6`I`L([=Ru{%1w* |8uj4ZXGm+{7-r3n');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
