<?php $this_pagetitle = get_term_by('slug',$term,$taxonomy)->name; ?>

<?php get_header(); ?>

<div id="btm_inner">
  <?php get_sidebar(); ?>

  <div class="btm_main_contens">

    <?php if(is_object_in_term('case_category' , 'store')): ?>
    <h2>施工事例　店舗<span>Case</span></h2>
    <?php elseif(is_object_in_term ('case_category' ,'apartment')): ?>
    <h2>施工事例　マンション<span>Case</span></h2>
    <?php elseif(is_object_in_term ('case_category' , 'house')): ?>
    <h2>施工事例　戸建て<span>Case</span></h2>
    <?php elseif(is_object_in_term ('case_category' , 'other')): ?>
    <h2>施工事例　その他<span>Case</span></h2>
    <?php endif; ?>

    <!--<h2>施工事例<span>Case</span></h2>-->
      <!--<p>只今準備中です</p>
      <img src="../common/img/case/comingsoon.jpg" height="423" width="650" alt="準備中">-->
      <p class="case_outline">一部の工事実績を掲載してます。記載価格は、塗装部位、塗装仕様により若干異なります。</p>

      <?php if (function_exists("pagination")) {
        pagination($additional_loop->max_num_pages);
      } ?>

      <?php if(have_posts()): while (have_posts()): the_post(); ?>
        <?php $postid = get_the_ID(); ?>
        <h3><a href="#<?php echo $postid; ?>"><?php the_title(); ?></a></h3>
        <div class="case_section<?php if(isLast()) echo '_last'; ?>">

        <?php
        $attachment_id = get_field('img_before');
        $size = "case_thum"; // (thumbnail, medium, large, full or custom size(array(32,32)のように))
        $image = wp_get_attachment_image_src( $attachment_id, $size );
        $attachment = get_post( get_field('img_before') );
        $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
        $image_title = $attachment->post_title;
        ?>

          <p class="before_img"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $alt; ?>" title="<?php echo $image_title; ?>" /><span>before</span></p>

        <?php
        $attachment_id = get_field('img_after');
        $size = "case_thum"; // (thumbnail, medium, large, full or custom size(array(32,32)のように))
        $image = wp_get_attachment_image_src( $attachment_id, $size );
        $attachment = get_post( get_field('img_after') );
        $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
        $image_title = $attachment->post_title;
        ?>

          <p class="after_img"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $alt; ?>" title="<?php echo $image_title; ?>" /><span>after</span></p>
          <dl>
            <dt>築年数：</dt>
            <dd><?php the_field('age'); ?></dd>
            <dt>外壁：</dt>
            <dd><?php the_field('wall'); ?></dd>
            <dt>建坪：</dt>
            <dd><?php the_field('tatetsubo'); ?></dd>
            <dt>費用：</dt>
            <dd><?php the_field('cost'); ?></dd>
            <dt>期間：</dt>
            <dd><?php the_field('term'); ?></dd>
            <dt class="last"><?php the_field('remark'); ?></dt>
          </dl>
        </div><!--/.case_section-->
        
      <?php endwhile; endif; ?>

      <?php if (function_exists("pagination")) {
        pagination($additional_loop->max_num_pages);
      } ?>

    </div><!--/.btm_main_contens-->
  </div><!--/#btm_inner-->

  <?php get_footer(); ?>