<?php 

function remove_footer_admin () {
  echo 'お問い合わせは<a href="http://www.officepartner.jp/contact/" target="_blank">オフィスパートナー株式会社</a>まで';
}
add_filter('admin_footer_text', 'remove_footer_admin');

if (!current_user_can('administrator')) {
  add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
}

if (!current_user_can('edit_users')) {
  function remove_menus () {
  global $menu;
  $restricted = array(
    __('リンク'),
    __('ツール'),
    __('コメント'),
    __('プロフィール')
  );
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
    unset($menu[key($menu)]);
    }
  }
  }
  add_action('admin_menu', 'remove_menus');
}
function example_remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
  //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

add_action( 'wp_before_admin_bar_render', 'hide_before_admin_bar_render' );
function hide_before_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );
}
// ウィジェット
//register_sidebar();

// メニューを非表示にする
function remove_menus02 () {
 if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
 remove_menu_page('wpcf7'); //Contact Form 7
 global $menu;
 unset($menu[5]); // 投稿
 unset($menu[20]);
 }
 }
add_action('admin_menu', 'remove_menus02');



function my_custom_login_logo() {
  echo '<style type="text/css">
  h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo-login.png) !important; }</style>';
  echo '
  <script type="text/javascript">

  </script>
  ';
}
add_action('login_head', 'my_custom_login_logo');
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
  $args['checked_ontop'] = false;
  return $args;
}

//wp_head()のいらないタグを削除
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head');

//wp_head()のいらないタグを削除(絵文字）
function disable_emoji() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'wp_shortlink_wp_head'); 
  remove_action('wp_head', 'rel_canonical');   
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

//固定ページのビジュアルエディタを非表示
function my_remove_post_editor_support() {
 remove_post_type_support( 'post', 'editor' );
}

function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}
function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

// カスタム投稿タイプを追加
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type('case',
    array(
      'label' => '施工事例',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 5,
      'supports' => array('title'),
      'labels' => array (
        'name' => '施工事例',
        'all_items' => '施工事例一覧'
      )
    )
  );

// カスタムタクソノミーの追加
  register_taxonomy(
    'case_category',  /* タクソノミーのslug */
    'case',           /* 属する投稿タイプ */
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => '施工事例カテゴリ',
      'singular_label' => '施工事例カテゴリ',
      'public' => true,
      'show_ui' => true
    )
  );
}

// 画像サイズの設定
add_theme_support('post-thumbnails');
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'case_thum', 199, 176, true );
}

// 記事の最初と最後の判定
function isFirst(){
    global $wp_query;
    return ($wp_query->current_post === 0);
}

function isLast(){
    global $wp_query;
    return ($wp_query->current_post+1 === $wp_query->post_count);
}

// 自動生成されるタグを制御
remove_filter ( 'the_content', 'wpautop' );
remove_filter ( 'the_excerpt', 'wpautop' );

/* 施工事例
-------------------------------------------------------------*/
function change_posts_per_page($my_query) {
  if( is_admin() || ! $my_query->is_main_query() ){
    return;
  }

  if($my_query -> is_post_type_archive('case')){
    $my_query->set( 'posts_per_page', '5' );
    return;
  }
}
add_action( 'pre_get_posts', 'change_posts_per_page' );


function taxonomy_page($my_query) {
  if( is_admin() || ! $my_query->is_main_query() ){
    return;
  }

  if($my_query -> is_tax( 'case', array( 'store', 'apartment', 'house','other' ) ) ){
    $my_query->set( 'posts_per_page', '5' );
    return;
  }
}
add_action( 'pre_get_posts', 'taxonomy_page' );


/* pagenation
-------------------------------------------------------------*/
function pagination($pages = '', $range = 9)
{
$showitems = 1; //($range * 2)+1; 

global $paged;
if(empty($paged)) $paged = 1;

if($pages == '')
{
global $wp_query;
$pages = $wp_query->max_num_pages;
if(!$pages)
{
$pages = 1;
}
} 

if(1 != $pages)
{
echo "<div class=\"pager-area\"><ul>";
//if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
if($paged > 1 && $showitems < $pages) echo "<li class=\"ba\"><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

for ($i=1; $i <= $pages; $i++)
{
if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
{
echo ($paged == $i)? "<li class=\"active\"><span class=\"current\">".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
}
}
if ($paged < $pages && $showitems < $pages) echo "<li class=\"ba\"><a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a></li>";
//if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
echo "</ul></div>\n";
}
}

?>