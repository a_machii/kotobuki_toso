  <div class="to_top">
    <a href="#header"><img src="<?php echo site_url('/'); ?>common/img/to_top.png" height="23" width="92" alt="トップへ戻る" class="ophv"></a>
  </div><!--/.to_top-->

  <div id="footer">
    <div class="inner">
      <ul>
          <li>
            <a href="<?php echo site_url('/'); ?>top" class="first"><img src="<?php echo site_url('/'); ?>common/img/gnav_top.png" height="36" width="86" alt="" class="ophv"></a>
          </li>
          <li>
            <a href="<?php echo site_url('/'); ?>our_service"><img src="<?php echo site_url('/'); ?>common/img/gnav_our_service.png" height="36" width="103" alt="" class="ophv"></a>
          </li>
          <li>
            <a href="<?php echo site_url('/'); ?>case"><img src="<?php echo site_url('/'); ?>common/img/gnav_case.png" height="36" width="58" alt="" class="ophv"></a>
          </li>
          <li>
            <a href="<?php echo site_url('/'); ?>about_us"><img src="<?php echo site_url('/'); ?>common/img/gnav_about_us.png" height="36" width="58" alt="" class="ophv"></a>
          </li>
          <li>
            <a href="<?php echo site_url('/'); ?>contact" class="last"><img src="<?php echo site_url('/'); ?>common/img/gnav_contact.png" height="36" width="86" alt="" class="ophv"></a>
          </li>
        </ul>
      <div class="f_info">
        <div class="f_logo">
          <p class="dial">0120-234-323</p>
          <p><a href="<?php echo site_url('/'); ?>"><img src="<?php echo site_url('/'); ?>common/img/f_logo.png" height="31" width="180" alt="有限会社寿塗装"></a></p>
        </div><!--/.f_logo-->
        <div class="f_infobox">
          <p>〒194-0004 東京都町田市鶴間228－1</p>
          <p class="tf">TEL <span>042-788-3640</span>  FAX <span>042-795-4154</span></p>
          <p>営業時間 8:00～18:00  年中無休で営業</p>
        </div><!--/.f_infobox-->
      </div><!--/.f_info-->
    </div><!--/.inner-->
  </div><!--/#footer-->

  <div class="copyright">
    <p>Copyright 2016(C) Kotobuki Painting Limited. All rights reserved.</p>
  </div><!--/.copyright-->

<?php wp_footer(); ?>
</body>
</html>
