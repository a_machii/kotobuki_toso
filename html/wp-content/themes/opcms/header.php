<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <?php
    global $this_pagetitle;
    global $this_page_keywd;
    global $this_page_desc;
  ?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php wp_title ( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
  <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>有限会社寿塗装は昭和30年創業から東京都町田市の地元はもちろん都内近郊において「素材が喜ぶ塗装を」経営理念とし、お子様を取り巻く環境作りから取り組んでいる寿塗装は、クライアント様の厳しい注文やどんな要望にも応えられる高い技術力で、 住む人が楽しく未来の可能性を広げられるように全力でサ ポートいたします。" />
  <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>町田市,八王子,塗装,ロハス,あきる野市,外壁,屋根,塗り替え" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
  <link rel="stylesheet" href="<?php echo site_url('/'); ?>common/css/style.css">
  <?php if (is_page('contact')): ?>
  <link rel="stylesheet" href="<?php echo site_url('/'); ?>/mailform/mfp.statics/mailformpro.css" type="text/css" />
  <?php endif ?>
  <script src="<?php echo site_url('/'); ?>common/js/jquery-1.12.2.min.js" type="text/javascript"></script>

  <!-- ロールオーバーの記述 -->
  <script src="<?php echo site_url('/'); ?>common/js/rollover/rollover.js" type="text/javascript"></script>
  <script src="<?php echo site_url('/'); ?>common/js/rollover/opacity-rollover2.1.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.ophv').opOver(1.0, 0.6, 200, 200);
    });
  </script>

  <!-- smoothScrollの記述 -->
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/jquery.smoothScroll.js"></script>
  <script>
    jQuery(function($) {
      $('a[href^="#"]').SmoothScroll({
        duration: 1000,
        easing: 'easeOutQuint'
      });
    });
  </script>

  <?php wp_head(); ?>
</head>

<body>
  <header>
    <div id="header">
      <div class="inner">
        <h1><a href="<?php echo site_url('/'); ?>"><img src="<?php echo site_url('/'); ?>common/img/logo.png" height="52" width="180" alt="有限会社寿塗装"></a></h1>
        <div class="info">
          <p>建築塗装に関する事なら有限会社寿塗装にお任せください | 東京町田市鶴間</p>
          <dl>
            <dt>0120-234-323</dt>
            <dd>営業時間 8:00～18:00 年中無休で営業</dd>
          </dl>
          <p class="contact">
            <a href="<?php echo site_url('/'); ?>contact"><img src="<?php echo site_url('/'); ?>common/img/contact_btn.png" alt="お問い合わせ" width="171" height="39" class="ophv"></a>
          </p>
          <div class="gnav">
            <ul>
              <li>
                <a href="<?php echo site_url('/'); ?>" class="first"><img src="<?php echo site_url('/'); ?>common/img/gnav_top.png" height="36" width="86" alt="" class="ophv"></a>
              </li>
              <li>
                <a href="<?php echo site_url('/'); ?>our_service"><img src="<?php echo site_url('/'); ?>common/img/gnav_our_service.png" height="36" width="103" alt="" class="ophv"></a>
              </li>
              <li>
                <a href="<?php echo site_url('/'); ?>case"><img src="<?php echo site_url('/'); ?>common/img/gnav_case.png" height="36" width="58" alt="" class="ophv"></a>
              </li>
              <li>
                <a href="<?php echo site_url('/'); ?>about_us"><img src="<?php echo site_url('/'); ?>common/img/gnav_about_us.png" height="36" width="58" alt="" class="ophv"></a>
              </li>
              <li>
                <a href="<?php echo site_url('/'); ?>contact" class="last"><img src="<?php echo site_url('/'); ?>common/img/gnav_contact.png" height="36" width="86" alt="" class="ophv"></a>
              </li>
            </ul>
          </div><!--/.gnav-->
        </div><!--/.info-->
      </div><!--/.inner-->
    </div><!--/#header-->
  </header>

  <?php if(is_home()): ?>
  <div id="main_img">
    <div class="inner">
      <h2><img src="<?php echo site_url('/'); ?>common/img/catchcopy.gif" height="34" width="336" alt="素材が喜ぶ塗装"></h2>
      <p>お子様を取り巻く環境作りから取り組んでいる寿塗装は、クライアント様の
        <br /> 厳しい注文やどんな要望にも応えられる高い技術力で、
        <br /> 住む人が楽しく未来の可能性を広げられるように全力でサポートいたします。
      </p>
    </div><!--/.inner-->
  </div><!--/#main_img-->
<?php endif; ?>
