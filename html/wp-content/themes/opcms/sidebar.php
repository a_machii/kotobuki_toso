
    <div class="sidebar">
      <p class="side_ttl">施工事例一覧</p>
      <div class="case_ichiran">
        <dl>
          <dt class="tenpo">店舗</dt>
          <?php
            $store = array(
              'post_type' => 'case',
              'tax_query' => array(
                array(
                  'taxonomy' => 'case_category',
                  'field' => 'slug',
                  'terms' => 'store'
                  )
                ),
              'posts_per_page' => 3,
            );
            $my_query = new WP_Query( $store );
            if($my_query->have_posts()): while ( $my_query->have_posts() ) : $my_query->the_post();
          ?>

          <?php $postid = get_the_ID(); ?>
          <dd><a href="<?php echo site_url('/'); ?>case_category/store#<?php echo $postid; ?>" class="ophv"><?php the_title(); ?></a></dd>
            <?php endwhile; endif; ?>
          <?php wp_reset_postdata(); ?>
        </dl>

        <dl>
          <dt class="manshon">マンション</dt>
          <?php
            $apartment = array(
              'post_type' => 'case',
              'tax_query' => array(
                array(
                  'taxonomy' => 'case_category',
                  'field' => 'slug',
                  'terms' => 'apartment'
                  )
                ),
              'posts_per_page' => 3,
            );
            $my_query = new WP_Query( $apartment );
            if($my_query->have_posts()): while ( $my_query->have_posts() ) : $my_query->the_post();
          ?>

          <?php $postid = get_the_ID(); ?>
          <dd><a href="<?php echo site_url('/'); ?>case_category/apartment#<?php echo $postid; ?>" class="ophv"><?php the_title(); ?></a></dd>
            <?php endwhile; endif; ?>
          <?php wp_reset_postdata(); ?>
        </dl>

        <dl>
          <dt class="kodate">戸建て</dt>
          <?php
            $house = array(
              'post_type' => 'case',
              'tax_query' => array(
                array(
                  'taxonomy' => 'case_category',
                  'field' => 'slug',
                  'terms' => 'house'
                  )
                ),
              'posts_per_page' => 3,
            );
            $my_query = new WP_Query( $house );
            if($my_query->have_posts()): while ( $my_query->have_posts() ) : $my_query->the_post();
          ?>

          <?php $postid = get_the_ID(); ?>
          <dd><a href="<?php echo site_url('/'); ?>case_category/house#<?php echo $postid; ?>" class="ophv"><?php the_title(); ?></a></dd>
            <?php endwhile; endif; ?>
          <?php wp_reset_postdata(); ?>
        </dl>

        <dl>
          <dt class="other">その他</dt>
          <?php
            $other = array(
              'post_type' => 'case',
              'tax_query' => array(
                array(
                  'taxonomy' => 'case_category',
                  'field' => 'slug',
                  'terms' => 'other'
                  )
                ),
              'posts_per_page' => 3,
            );
            $my_query = new WP_Query( $other );
            if($my_query->have_posts()): while ( $my_query->have_posts() ) : $my_query->the_post();
          ?>

          <?php $postid = get_the_ID(); ?>
          <dd><a href="<?php echo site_url('/'); ?>case_category/other#<?php echo $postid; ?>" class="ophv"><?php the_title(); ?></a></dd>
            <?php endwhile; endif; ?>
          <?php wp_reset_postdata(); ?>
        </dl>
      </div><!--/.case_ichiran-->

      <div class="side_banner">
        <p><a href="<?php echo site_url('/'); ?>our_service/#strength"><img src="<?php echo site_url('/'); ?>common/img/side_banner01.jpg" height="66" width="284" alt="寿塗装3つの強み" class="ophv"></a></p>
        <p class="reason"><a href="<?php echo site_url('/'); ?>our_service/#reason"><img src="<?php echo site_url('/'); ?>common/img/side_banner02.jpg" height="66" width="284" alt="外壁の塗替えが必要な理由" class="ophv"></a></p>
        <p><a href="<?php echo site_url('/'); ?>our_service/#caution"><img src="<?php echo site_url('/'); ?>common/img/side_banner03.jpg" height="66" width="284" alt="こんな症状が見られたら注意" class="ophv"></a></p>
        <p class="request"><a href="<?php echo site_url('/'); ?>our_service/#request"><img src="<?php echo site_url('/'); ?>common/img/side_banner04.jpg" height="116" width="284" alt="依頼から着工までの流れ" class="ophv"></a></p>
        <p><a href="<?php echo site_url('/'); ?>our_service/#flow"><img src="<?php echo site_url('/'); ?>common/img/side_banner05.jpg" height="116" width="284" alt="工事のの流れ" class="ophv"></a></p>
      </div><!--/.side_banner-->

      <div class="side_contact">
        <p>お気軽にお問い合わせ下さい</p>
        <dl>
          <dt>0120-234-323</dt>
          <dd>営業時間 8:00～18:00  年中無休で営業</dd>
        </dl>
        <p class="side_btn"><a href="<?php echo site_url('/'); ?>contact"><img src="<?php echo site_url('/'); ?>common/img/side_contact_btn.png" height="44" width="254" alt="お問い合わせはこちらから" class="ophv"></a></p>
      </div><!--/.side_info-->
    </div><!--/.sidebar-->
