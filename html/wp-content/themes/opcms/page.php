<?php /* Template Name: page */ ?>

<?php
$page_id = $post->ID; //xxxに 固定ページIDを入力
$content = get_post($page_id);
$this_pagetitle = $content->post_title;
?>

<?php get_header(); ?>

  <div id="btm_inner">
    <?php get_sidebar(); ?>

    <div class="btm_main_contens">
      <?php if(have_posts()): while (have_posts()): the_post();?>
        <?php the_content(); ?>
      <?php endwhile; endif; ?>
    </div><!--/.btm_main_contens-->
  </div><!--/#btm_inner-->

  <?php get_footer(); ?>