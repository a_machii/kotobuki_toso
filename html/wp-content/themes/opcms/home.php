<?php get_header(); ?>

  <div id="main_contents">
    <div class="inner">
      <div class="service">
        <ul>
          <li>
            <dl>
              <dt><img src="<?php echo site_url('/'); ?>common/img/os_01.jpg" height="103" width="304" alt="強み1"></dt>
              <dd class="strong">強み<span>1</span></dd>
              <dd class="cap">昭和30年創業、豊富な実績。</dd>
              <dd class="text">昭和30年の創業時より地域の皆様に支えられながら、数多くの新築リフォーム・改修工事・大規模修繕工事などのお手伝いをしてまいりました。</dd>
              <dd class="btn"><a href="<?php echo site_url('/'); ?>our_service/#first"><img src="<?php echo site_url('/'); ?>common/img/detail_btn.png" height="32" width="160" alt="詳しく見る" class="ophv"></a></dd>
            </dl>
          </li>
          <li class="center">
            <dl>
              <dt><img src="<?php echo site_url('/'); ?>common/img/os_02.jpg" height="103" width="304" alt="強み2"></dt>
              <dd class="strong">強み<span>2</span></dd>
              <dd class="cap">１級免許を持った職人が<br />きめ細やかなサービスを提供。</dd>
              <dd class="text">塗装工事のプロフェッショナルである1級塗装技能士や高所作業車運転主任、他塗装にかかわる資格保有者が多く在籍しています。</dd>
              <dd class="btn"><a href="<?php echo site_url('/'); ?>our_service/#second"><img src="<?php echo site_url('/'); ?>common/img/detail_btn.png" height="32" width="160" alt="詳しく見る" class="ophv"></a></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt><img src="<?php echo site_url('/'); ?>common/img/os_03.jpg" height="103" width="304" alt="強み3"></dt>
              <dd class="strong">強み<span>3</span></dd>
              <dd class="cap">口に含んでも安心の<br/>オスモカラー塗料を使用。</dd>
              <dd class="text">他の自然塗料とは違い高品質な植物油を使っているので、乾燥時間や仕上がり、耐久性において国交省が定める品質試験に合格している塗料です。</dd>
              <dd class="btn"><a href="<?php echo site_url('/'); ?>our_service/#third"><img src="<?php echo site_url('/'); ?>common/img/detail_btn.png" height="32" width="160" alt="詳しく見る" class="ophv"></a></dd>
            </dl>
          </li>
        </ul>
      </div><!--/.service-->

      <div class="message">
        <div class="mes_text">
          <h2>外壁塗装なら、昭和30年創業・確かな技術と信頼の品質の寿塗装へ。</h2>
          <p>外壁塗装や塗り替えリフォーム・改修工事・屋根塗装の事なら、東京都町田市の寿塗装にお任せください。寿塗装には経験豊富な一級塗装技能士をはじめとし、高所作業車運転主任・職業訓練指導員など塗装にかかわる資格保有者が多く在籍しておりますので、安心して作業をお任せ下さい。塗装は内外壁にかかわらず、階段や天井はもちろんの事、障子の格子枠・床・建具類やキッチンの収納部まであらゆる範囲でお受けいたします。</p>
        </div><!--/.mes_text-->

        <div class="mes_photo">
          <img src="<?php echo site_url('/'); ?>common/img/mes_photo.jpg" height="175" width="296" alt="メッセージ写真">
        </div><!--/.mes_photo-->
      </div><!--/.messsage-->
    </div><!--/.inner-->
  </div><!--/#main_contents-->

  <?php get_footer(); ?>
